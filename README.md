# XLR 22.3.14 (existant)

## Prérequis

- kubernetes 1.25.*	
- https://github.com/xebialabs/xl-kube-workshop (utiliser 22.3.14 comme image)

## xl kube install

- suivre les instructions du tutoriel
    - NB: ne pas remplacer la CRD qui est partagée par TOUS les utilisateurs de Kube
		
# XLR 23.3.0 (voulu)

## Prérequis

- kubernetes 1.27.*
		
## xl kube upgrade

## xl kube install (remote runner-only)

https://docs.digital.ai/bundle/devops-release-version-v.23.3/page/release/operator/xl-op-install-wizard-remote-runner.html
	

# Migration

## Prérequis

- kubernetes 1.25.*
- xl cli 23.3.0 (prendre la version correspondant à celle où l'on souhaite migrer)

		